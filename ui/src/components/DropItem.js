import React, { Component } from "react";
import { DropdownItem } from "reactstrap";
import { connect } from "react-redux";

import { updateServers } from "../actions/";
import servers from "../API/API.js";

const mapDipsatchToProps = dispatch => {
  return {
    updateServers: servers => dispatch(updateServers(servers))
  };
};

class DItem extends Component {
  constructor(props) {
    super(props);
    this.state = {
      serverStatus: "REBOOTING"
    };
  }

  findServerById(server, serverId) {
    function checkId(s) {
      return s.id === serverId;
    }
    return server.find(checkId);
  }

  getSeverStatusById(servers, id) {
    const server = this.findServerById(servers, id);
    return server.status;
  }

  handleClick = () => {
    if (this.props.txt === "Turn on") {
      servers
        .servers()
        .update(this.props.server.id, "on")
        .then(() =>
          servers
            .servers()
            .getAll()
            .then(res => {
              const servers = res.data;
              this.props.updateServers(servers);
            })
        );
    } else if (this.props.txt === "Turn off") {
      servers
        .servers()
        .update(this.props.server.id, "off")
        .then(() =>
          servers
            .servers()
            .getAll()
            .then(res => {
              const servers = res.data;
              this.props.updateServers(servers);
            })
        );
    } else if (this.props.txt === "Reboot")
      servers
        .servers()
        .update(this.props.server.id, "reboot")
        .then(() => {
          servers
            .servers()
            .getAll()
            .then(res => {
              const servers = res.data;
              this.state = {
                serverStatus: this.getSeverStatusById(
                  servers,
                  this.props.server.id
                )
              };
              this.props.updateServers(servers);
            });
          let interval = null;
          interval = setInterval(() => {
            servers
              .servers()
              .getAll()
              .then(res => {
                const servers = res.data;
                this.state = {
                  serverStatus: this.getSeverStatusById(
                    servers,
                    this.props.server.id
                  )
                };
                this.props.updateServers(servers);
              });

            if (this.state.serverStatus !== "REBOOTING") {
              clearInterval(interval);
            }
          }, 1000);
        });
  };

  render() {
    return (
      <DropdownItem onClick={this.handleClick}>{this.props.txt}</DropdownItem>
    );
  }
}

const DropItem = connect(
  null,
  mapDipsatchToProps
)(DItem);

export default DropItem;
