import React, { Component } from "react";

class Header extends Component {
  render() {
    return (
      <div>
        <p className="bold600 pHeader">Servers</p>
        <p className="pHeaderSm">
          Number of elements: {this.props.serverLength}
        </p>
      </div>
    );
  }
}

export default Header;
