import React, { Component } from "react";

class Logo extends Component {
  render() {
    return (
      <div className="logo colorWhite">
        <div className="elipsa-1" />
        Recruitment task
      </div>
    );
  }
}

export default Logo;
