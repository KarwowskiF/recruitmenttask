import React, { Component } from "react";
import { connect } from "react-redux";

import TableRow from "./TableRow.js";

const mapStateToProps = state => {
  return { servers: state.servers, searchWord: state.searchWord };
};

class TBody extends Component {
  getServer() {
    return this.props.servers;
  }

  render() {
    const servers = this.getServer();
    if (servers.length === 0) return null;
    else if (this.props.searchWord === "") {
      return (
        <tbody>
          {servers[0].map(server => (
            <TableRow key={server.id} server={server} />
          ))}
        </tbody>
      );
    } else if (this.props.searchWord !== "") {
      const tab = [];
      servers[0].map(server => {
        if (server.name.toLowerCase().search(this.props.searchWord) >= 0)
          tab.push(server);
      });
      return (
        <tbody>
          {tab.map(server => (
            <TableRow key={server.id} server={server} />
          ))}
        </tbody>
      );
    }
  }
}

const TableBody = connect(mapStateToProps)(TBody);

export default TableBody;
