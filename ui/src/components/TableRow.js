import React, { Component } from "react";

import Dropdown from "./Dropdown.js";

class TableRow extends Component {
  constructor(props) {
    super(props);
    this.handleStatusChange = this.handleStatuschange.bind(this);
    this.state = "";
  }

  handleStatuschange(status) {
    this.setState({
      status: status
    });
  }

  render() {
    return (
      <tr className={this.props.server.status.toLowerCase()}>
        <th scope="row">{this.props.server.name}</th>
        <td>
          <div className="status">
            <i className="fas fa-times" />
          </div>
          <span>{this.props.server.status}</span>
        </td>
        <td>
          <Dropdown
            onStatusChange={this.handleStatusChange}
            server={this.props.server}
          />
        </td>
      </tr>
    );
  }
}

export default TableRow;
