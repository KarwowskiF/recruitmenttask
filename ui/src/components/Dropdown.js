import React, { Component } from "react";
import { ButtonDropdown, DropdownToggle } from "reactstrap";

import DropMenu from "./DropMenu.js";

class Dropdown extends Component {
  constructor(props) {
    super(props);

    this.handleChange = this.handleChange.bind(this);
    this.toggle = this.toggle.bind(this);
    this.state = {
      dropdownOpen: false
    };
  }

  handleChange() {
    this.props.onStatusChange("change");
  }

  toggle() {
    this.setState({
      dropdownOpen: !this.state.dropdownOpen
    });
  }

  render() {
    return (
      <ButtonDropdown
        onClick={this.handleChange}
        isOpen={this.state.dropdownOpen}
        toggle={this.toggle}
      >
        <DropdownToggle
          className="btnDrop"
          style={{
            borderTopRightRadius: "20px",
            borderBottomRightRadius: "20px"
          }}
        >
          <span className="dots">...</span>
        </DropdownToggle>
        <DropMenu server={this.props.server} />
      </ButtonDropdown>
    );
  }
}

export default Dropdown;
