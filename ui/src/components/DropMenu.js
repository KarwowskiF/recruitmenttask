import React, { Component } from "react";
import { DropdownMenu } from "reactstrap";

import DropItem from "./DropItem";

class DropMenu extends Component {
  render() {
    let menu;
    const isOffline = this.props.server.status === "OFFLINE";
    if (isOffline) {
      menu = (
        <DropdownMenu>
          <DropItem server={this.props.server} txt="Turn on" />
        </DropdownMenu>
      );
    } else {
      menu = (
        <DropdownMenu>
          <DropItem server={this.props.server} txt="Turn off" />
          <DropItem server={this.props.server} txt="Reboot" />
        </DropdownMenu>
      );
    }

    return menu;
  }
}

export default DropMenu;
