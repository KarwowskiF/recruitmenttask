import React, { Component } from "react";

import TableHead from "./TableHead.js";
import TableBody from "./TableBody.js";

class Table extends Component {
  render() {
    return (
      <table className="table">
        <TableHead />
        <TableBody />
      </table>
    );
  }
}

export default Table;
