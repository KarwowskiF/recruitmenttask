import React, { Component } from "react";
import { connect } from "react-redux";

import { updateSearchWord } from "../actions/";

const mapDipsatchToProps = dispatch => {
  return {
    updateSearchWord: searchWord => dispatch(updateSearchWord(searchWord))
  };
};

class SrchInput extends Component {
  constructor(props) {
    super(props);
    this.handleInputChange = this.handleInputChange.bind(this);
  }

  handleInputChange(e) {
    this.props.updateSearchWord(e.target.value.toLowerCase());
  }

  render() {
    return (
      <div className="inputBox">
        <i className="fas fa-search" />
        <input
          onChange={this.handleInputChange}
          type="text"
          placeholder="Search"
        />
      </div>
    );
  }
}

const SearchInput = connect(
  null,
  mapDipsatchToProps
)(SrchInput);

export default SearchInput;
