import React, { Component } from "react";

class TableHead extends Component {
  render() {
    return (
      <thead>
        <tr>
          <th className="colName" scope="col">
            NAME
          </th>
          <th className="colStatus" scope="col">
            STATUS
          </th>
          <th className="colBtn" scope="col" />
        </tr>
      </thead>
    );
  }
}

export default TableHead;
