import React, { Component } from "react";
import Logo from "./Logo";

class Navigation extends Component {
  render() {
    return (
      <nav className="navbar align-items-start nav-style">
        <Logo />
      </nav>
    );
  }
}
export default Navigation;
