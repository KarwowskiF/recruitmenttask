import axios from "axios";

export default {
  servers(url = "http://127.0.0.1:4454/servers") {
    return {
      getOne: id => axios.get(`${url}/${id}`),
      getAll: () => axios.get(url),
      update: (id, status) => axios.put(`${url}/${id}/${status}`),
      delete: id => axios.delete(`${url}/${id}`)
    };
  }
};
