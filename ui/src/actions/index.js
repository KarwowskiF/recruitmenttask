export const addServers = servers => ({
  type: "INIT_SERVERS",
  servers: servers
});
export const updateServers = servers => ({
  type: "UPDATE_SERVERS",
  servers: servers
});
export const updateSearchWord = searchWord => ({
  type: "UPDATE_SEARCH_WORD",
  searchWord: searchWord
});
