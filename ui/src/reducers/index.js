const initialState = {
  servers: [],
  searchWord: ""
};

const rootReducer = (state = initialState, action) => {
  switch (action.type) {
    case "INIT_SERVERS":
      return { ...state, servers: [...state.servers, action.servers] };
    case "UPDATE_SERVERS":
      return { ...state, servers: [action.servers] };
    case "UPDATE_SEARCH_WORD":
      return { ...state, searchWord: action.searchWord };
    default:
      return state;
  }
};

export default rootReducer;
