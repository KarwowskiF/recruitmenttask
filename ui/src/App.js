import React, { Component } from "react";

import Navigation from "./components/Navigation.js";
import Content from "./containers/Content.js";

class App extends Component {
  render() {
    return (
      <div>
        <Navigation />
        <Content />
      </div>
    );
  }
}

export default App;
