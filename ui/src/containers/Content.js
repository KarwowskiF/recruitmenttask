import React, { Component } from "react";
import { connect } from "react-redux";

import servers from "../API/API.js";
import SearchInput from "../components/SearchInput.js";
import Header from "../components/Header.js";
import Table from "../components/Table.js";
import { addServers } from "../actions/";

const mapDipsatchToProps = dispatch => {
  return {
    addServers: servers => dispatch(addServers(servers))
  };
};

class ContentContainer extends Component {
  constructor(props) {
    super(props);
    this.state = {
      serversLength: 0
    };
  }
  componentDidMount() {
    servers
      .servers()
      .getAll()
      .then(res => {
        const servers = res.data;
        this.setState({ serversLength: servers.length });
        this.props.addServers(servers);
      });
  }

  render() {
    return (
      <div className="container">
        <div className="row mt-4 mb-1">
          <div className="col-4 ">
            <Header serverLength={this.state.serversLength} />
          </div>
          <div className="col-3 offset-5 my-auto">
            <SearchInput />
          </div>
        </div>
        <div className="row">
          <div className="col">
            <Table />
          </div>
        </div>
      </div>
    );
  }
}

const Content = connect(
  null,
  mapDipsatchToProps
)(ContentContainer);

export default Content;
